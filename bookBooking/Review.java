import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Review
 */
public class Review {
  Scanner console = new Scanner(System.in);
  Color print = new Color();

  public void addingReview() throws IOException {
    String[] bookNames = new String[]{};
    Table st = new Table();
    st.setShowVerticalLines(true);

    String line = "";
    String splitBy = ",";
    try {
      BufferedReader list = new BufferedReader(
      new FileReader(System.getProperty("user.dir") + "/books.csv" ));
      String tableHeader = list.readLine();
      String[] headerName = tableHeader.split(splitBy);
      st.setHeaders("#",headerName[0],headerName[1], headerName[2]);
      Integer counter = 0;
      while ((line = list.readLine()) != null) {
        String[] item = line.split(splitBy);
        counter++;
        bookNames = ArrayHelper.push(bookNames, item[0]);
        st.addRow(""+counter,item[0],item[1],item[2]);
      }
      list.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    st.print();

    String bookID = console.nextLine();
    String bookName = bookNames[Integer.parseInt(bookID) -1];
    print.inputLabel("What your review for this book ?\n");
    String comment = console.nextLine();
    
    FileWriter path = new FileWriter(System.getProperty("user.dir") + "/review.csv",true);
    PrintWriter reviews = new PrintWriter(path);
    reviews.println(bookName + "," + comment);
    reviews.close();
  }

  public void menu() {
    Boolean isIncorrect = true;
    String choice = "";
    while(isIncorrect){
      print.warningLabel("========== ");
      print.normalLabel("Review Option");
      print.warningLabel(" ==========\n");
      print.inputLabel("1. Add new Review\t");      
      print.inputLabel("2. View Review List\t");      
      print.inputLabel("3. Back To Menu\n");      
      print.warningLabel("Enter your option : ");      
      choice = console.nextLine();
      if (choice.equals("1") || choice.equals("2")){
        isIncorrect = false;
      }else if(choice.equals("3")){
        isIncorrect = false;       
      }
    }
    if (choice.equals("1")) {
      try {
        addingReview();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }else if (choice.equals("2")){
      Table st = new Table();
      st.setShowVerticalLines(true);

      String line = "";
      String splitBy = ",";
      try {
        BufferedReader list = new BufferedReader(new FileReader(System.getProperty("user.dir") + "/review.csv" ));
        String tableHeader = list.readLine();
        String[] headerName = tableHeader.split(splitBy);
        st.setHeaders("#",headerName[0],headerName[1]);
        Integer counter = 0;
        while ((line = list.readLine()) != null) {
          String[] item = line.split(splitBy);
          counter++;
          st.addRow(""+counter,item[0],item[1]);
        }
        list.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
      st.print();
    }
  }

}