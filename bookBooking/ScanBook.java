
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;


/**
 * ScanBook
 */
public class ScanBook {
  Color print = new Color();
  Scanner console = new Scanner(System.in);
  String[] bookNames = new String[] {};

  public void addBook() throws IOException {
    print.inputLabel("Book Name : ");
    String name = console.nextLine();
    print.inputLabel("Author Name : ");
    String author = console.nextLine();

    // get current date
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    LocalDateTime now = LocalDateTime.now();
    // System.out.println(dtf.format(now));

    FileWriter path = new FileWriter(System.getProperty("user.dir") + "/books.csv", true);
    PrintWriter reviews = new PrintWriter(path);
    reviews.println(name + "," + author + "," + dtf.format(now) + ",-,-");
    reviews.close();
  }

  public void addBorrower() throws IOException {
    printBooks();
    print.inputLabel("Which book is borrowed : ");
    String bookID = console.nextLine();
    print.inputLabel("Name of Borrower : ");
    String borrower = console.nextLine();
    // set date
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    LocalDateTime nextWeek = LocalDateTime.now().plusWeeks(1);
    // saving
    String line = "";
    String[] booksStore = new String[]{};
    BufferedReader list = new BufferedReader(new FileReader(System.getProperty("user.dir") + "/books.csv"));
    while ((line = list.readLine()) != null) {
      booksStore = ArrayHelper.push(booksStore,line);
    }
    list.close();
    
    String temp = booksStore[Integer.parseInt(bookID)];
    String[] tempArr = temp.split(",");
    tempArr[3] = borrower;
    tempArr[4] = dtf.format(nextWeek);
    booksStore[Integer.parseInt(bookID)] = tempArr[0] + "," + tempArr[1] + "," + tempArr[2] + "," + tempArr[3] + "," + tempArr[4] ;
    // edit data done
    // saving back to file
    PrintWriter booksFile = new PrintWriter(System.getProperty("user.dir") + "/books.csv");
    for (int i = 0; i < booksStore.length; i++) {
      booksFile.println(booksStore[i]);
    }
    booksFile.close();
    // done saving
  }

  public void printBooks() {
    Table st = new Table();
    st.setShowVerticalLines(true);

    String line = "";
    String splitBy = ",";
    try {
      BufferedReader list = new BufferedReader(
      new FileReader(System.getProperty("user.dir") + "/books.csv" ));
      String tableHeader = list.readLine();
      String[] headerName = tableHeader.split(splitBy);
      st.setHeaders("#",headerName[0],headerName[1], headerName[2],headerName[3],headerName[4]);
      Integer counter = 0;
      while ((line = list.readLine()) != null) {
        String[] item = line.split(splitBy);
        counter++;
        st.addRow(""+counter,item[0],item[1],item[2],item[3],item[4]);
      }
      list.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    st.print();
  }

  
}