
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * Driver
 */
public class Driver {
  Color print = new Color();
  Scanner console = new Scanner(System.in);

  public static void main(String[] args) {
    Driver temp_driver = new Driver();
    temp_driver.driver();
  }

  public void driver() {
    Boolean isExit = true;
    do {
      print.clearConsole();

      try {
        System.out.print(banner());
        print.successLabel("\n\t\t\t  Booking System\n");
      } catch (Exception e) {
        System.out.println(" Welcome to Library Book Borrowing System\n");
        print.warningLabel("+++++++++++++++++++++++++++++++++++++++++++\n");
      }

      Boolean isIncorrect = true;
      String choice = "";
      while (isIncorrect) {
        print.warningLabel("========== ");
        print.normalLabel("Menu");
        print.warningLabel(" ==========\n");
        print.inputLabel("1. Review Option\t");
        print.inputLabel("2. Add new book\n");
        print.inputLabel("3. Scan book for borrowing\t");
        print.inputLabel("4. View Book Catalogue\n");
        print.inputLabel("5. Penalty\n");
        print.inputLabel("6. Exit Program\n");
        print.warningLabel("Enter Your Option : ");
        choice = console.nextLine();
        if (choice.equals("1") || choice.equals("2") || choice.equals("3") || choice.equals("4") || choice.equals("5")) {
          isIncorrect = false;
        } else if (choice.equals("6")) {
          isExit = false;
          isIncorrect = false;
        }
      }

      if (choice.equals("1")) {
        Review review = new Review();
        review.menu();
        
        isExit = holder();

      } else if (choice.equals("2")) {
        ScanBook scanBook = new ScanBook();
        try {
          scanBook.addBook();
        } catch (IOException e) {
          e.printStackTrace();
        }
        isExit = holder();

      } else if (choice.equals("3")) {
        ScanBook scanBook = new ScanBook();
        try {
          scanBook.addBorrower();
        } catch (IOException e) {
          e.printStackTrace();
        }
        isExit = holder();
      }else if (choice.equals("5")){
        Penalty penalty = new Penalty();
        penalty.checkPenalty();
      }else if (choice.equals("4")){
        ScanBook scanBook = new ScanBook();
        scanBook.printBooks();
      }
    }while(isExit);
  }

  public static String banner() throws Exception {
    String path = System.getProperty("user.dir") + "/banner.txt";
    return new String(Files.readAllBytes(Paths.get(path)));
  }

  public Boolean holder() {
    Boolean choice = false;
    print.inputLabel("Continue ? [ Y | N] ");
    String temp_x = console.nextLine();
    if (temp_x.equals("Y") || temp_x.equals("y")){
      choice = true;
    }
    return choice;
  } 
}