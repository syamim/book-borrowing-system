

/**
 * Color
 */
public class Color {
  public void inputLabel(String value){
    System.out.print("\033[0;96m -> " + "\033[1;34m" + value + "\033[0m");
  }
  
  public void warningLabel(String value){
    System.out.print("\033[0;93m" + value + "\033[0m");
  }
  
  public void errorLabel(String value){
    System.out.print("\033[0;91m" + value + "\033[0m");
  }
  
  public void normalLabel(String value){
    System.out.print("\033[0m" + value + "\033[0m");
  }
  
  public void successLabel(String value){
    System.out.print("\033[0;92m" + value + "\033[0m");
  }
  
  public void clearConsole(){
    System.out.print("\033[H\033[2J");  
    System.out.flush();
  }
}